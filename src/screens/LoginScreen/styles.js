import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    container: {
        justifyContent: 'center',
    }
});

export default style;